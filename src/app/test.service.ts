import {EventEmitter, Injectable, Output} from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class TestService {
  @Output() alertValueChanged: EventEmitter<any> = new EventEmitter();

  constructor() { }

  showAlert() {
    this.alertValueChanged.emit(true);
  }

  closeAlert() {
    this.alertValueChanged.emit(false);
  }

  getAlert() {
    return this.alertValueChanged;
  }
}
