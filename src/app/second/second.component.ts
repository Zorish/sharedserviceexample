import { Component, OnInit } from '@angular/core';
import {TestService} from '../test.service';

@Component({
  selector: 'app-second',
  templateUrl: './second.component.html',
  styleUrls: ['./second.component.css']
})
export class SecondComponent implements OnInit {
  public showAlert = false;

  constructor(private testService: TestService) { }

  ngOnInit() {
    this.testService.getAlert().subscribe((data) => {
      this.showAlert = data;
    });
  }

  closeAlert() {
    this.testService.closeAlert();
  }

}
