import { Component, OnInit } from '@angular/core';
import { TestService } from '../test.service';

@Component({
  selector: 'app-first',
  templateUrl: './first.component.html',
  styleUrls: ['./first.component.css']
})
export class FirstComponent implements OnInit {

  constructor(private testService: TestService) { }

  ngOnInit() {
  }

  showAlert() {
    this.testService.showAlert();
  }

}
